<img src="images/purdue-cs-logo.jpg" alt="drawing" width="450"/>

# Reproducing "A Deep Reinforcement Learning Perspective on Internet Congestion Control"

**Members:** [Shikun Liu](https://www.linkedin.com/in/shikun-liu-4074551b7/), [Tianchun Li](https://www.linkedin.com/in/tianchun-li-28b4521b7/), [Xinmeng Sun](https://www.linkedin.com/in/xinmeng-sun-320078177/), [Yunyu Liu](https://wenwen0319.github.io/), and [Nan Jiang](https://www.linkedin.com/in/jiangnanhugo/?_ga=2.216549008.784613071.1620663105-87289359.1620663105)

Since networks vary greatly in sizes, link capacities, network latency, level of composition between connections, etc. It is still a challenging task to identify the most optimal congestion control scheme. To solve the problem, the paper presents a novel deep reinforcement learning model for Internet congestion control. 

Basically, a Performance-oriented Congestion Control (PCC) is used to observe the performance of utility and adjust the sending rate to maximize link capacity. Reward function is used, which puts throughput, latency, and loss rates, into considerations. Also, reward, send rate, throughput, latency, and loss rate are observed during training process, as shown in the Figure 1. During testing process, real Linux kernel network emulation is utilized to ensure the performance of the model in real world application. As concluded by the authors, the RL based algorithm can perform significantly better than a traditional TCP approach. 

We further conduct some ablation studies to explore how the reward function benefits our model. In Figure 2, we modify the reward function by amplify the coefficients of the latency and loss rate 10 times, while Figure 1 shows the reproduced model with the original reward function that proposed in the paper. From the figures, we found that as the reward increasing, there is little change in the send rate, throughput, latency and loss rate. Although compared with the original reward function, the latency is about 1/2 of the original reward function, the loss rate is about the same, and the throughput also decrease to 1/3 . Therefore, we draw the conclusion that the model is sensitive to the  reward function. Different reward functions can lead to different results. In addition, the trade-off between throughput, latency and loss rate is not able to be solved by the naive reward function.

<p float="left"><br>
    <img src="images/env_graph_train_new.png" alt="Figure 1" width="500"/>
    <img src="images/env_graph_train_10latency.png" alt="Figure 2" width="500"/>
</p>
