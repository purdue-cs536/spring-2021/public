<img src="../others/images/purdue-cs-logo.jpg" alt="drawing" width="450"/>

# CS 536 Course Projects @ Purdue University (Spring 2021)

1. [**Offline and Online Learning for Anomaly Detection in Computer Network**](https://networkanomalydetection.wordpress.com/)
2. [**On Sparsification of Graph Networks**](8) 
3. [**Implementation and Design of Modbus over QUIC Protocol for ICS Networks**](47)
4. [**Sweeping Minesweeper to Make Mine**](https://njayinthehouse.github.io/blog/2021/03/23/sweep)
5. [**Network Dataset Generation Using GANs**](81)
6. [**Reproducing “Neural Packet Routing”**](32)
7. [**Reproducing "HTTP over UDP: An Experimental Investigation of QUIC"**](46)
8. [**Reproducing "Low-Rate TCP-Targeted Denial-of-Service Attacks"**](61)
9. [**Reproducing "A Deep Reinforcement Learning Perspective on Internet Congestion Control"**](4)
