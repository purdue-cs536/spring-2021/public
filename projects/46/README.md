<img src="images/purdue-cs-logo.jpg" alt="drawing" width="450"/>

# An Analysis of QUIC: Reproducing "HTTP over UDP: An Experimental Investigation of QUIC"

**Members:** [Aahan Aggarwal](https://www.linkedin.com/in/aahanaggarwal99/), [Arjun Arunasalam](https://www.linkedin.com/in/arjun-arunasalam-93701414a/), [Veronica Avisseh](https://www.linkedin.com/in/veronica-avisseh-b251a836/), [Eric Han](https://www.linkedin.com/in/eric-han-48a665201/), and [Mark Mikhail](https://www.linkedin.com/in/mark-mikhail-5745081aa/)

### Introduction
Across the years, the HTTP protocol has gone through a few iterations, with each new iteration hoping to improve on the previous one. Both the HTTP protocol and SPDY protocol use TCP in the transport layer to send and receive packets. In 2012, Google introduced the QUIC protocol as an alternative to HTTP and SPDY.   

<p float="center" >
  <img src="images/httpOverview.jpg" alt="HTTP Overview" width="400"/>   
  <img src="images/quicImage.jpg"  alt="QUIC Design" width="400"/> 
</p>

Interestingly, QUIC not only differs from HTTP by using multiplexing to support more packets, but also differs from SPDY by using the UDP protocol. To overcome UDPs inability to guarantee packet delivery, the QUIC protocol implements retransmission and congestion control at the application layer.   

The goal of our project is to reproduce the research produced in the aforementioned paper while also contributing new data by taking measurements with different parameters as well as introducing new implementations to the project methodology.

### Methodology
In order to set up the multiple servers that were used in the experimentation, we used AWS instances. This improves on the original paper's implementation where 2 laptops were used across a single network---using AWS instances increases the results relevancy, especially with respect to the cloud computing paradigm. We used APACHE to set up HTTP and SPDY servers while using dummy QUIC client and server provided by Google to run our experiments. To measure the performance of these protocols across different situation we used *tcconfig* to introduce random losses and bandwidth caps between connections. We used a variety of tools to obtain measurements including *ss*, *time*, *wget* terminal commands.


### Discussion
In no loss situations, QUIC shows improvement over both HTTP and SPDY for page-load times. Although this is not apparent from the bar graph below (on the left), we employ a paired-t test to test for statistical significance. In all use cases, we determine that QUIC is in fact able to improve on page-load times. This improvement grows in lossy situation (image on the right). We determine that packet retranmission on the application layer is able to cut down time in this case. In contrast to the original paper, we conduct our experiments with higher and lower bandwidths, higher loss situations, and also a more varied set of HTML pages (bigger pictures, plain text, and video files). In general, QUIC has better goodput and consistency.

<p float="center" >
  <img src="images/200kbps.jpg" width="300" />
  <img src="images/10mbps1.5loss.png" width="300" /> 
</p> 


### Conclusion
We observe that QUIC not only outperforms HTTP and SPDY in low bandwidth situations, but especially in lossy situations. Although many servers and websites have yet to adopt QUIC and occasionaly discourage it, we believe that the improvements reproduced in the original paper and the new improvements recorded in our paper serve as proof that QUIC should be adopted. It also seems that the IETF agrees since their draft of HTTP/3 uses QUIC as the transportation layer protocol.
