<img src="../../others/images/purdue-cs-logo.jpg" alt="drawing" width="450"/>

# On Sparsification of Graph Networks

**Members:** [Himanshi Mehta](https://himanshimehtap.wordpress.com/), [Aaditya Kharel](https://www.linkedin.com/in/aadityakharel/), [David Sundararaj Deepak Augustine Edwin](https://www.cs.purdue.edu/people/graduate-students/ddeepaka.html), [Ravi Tej Akella](https://www.linkedin.com/in/akella17/?originalSubdomain=in), and [Sonali Srijan](https://www.linkedin.com/in/sonali-srijan/)

The goal of this project is to motivate the study of efficient algorithms for the k-preserving problem. This problem has been long studied from a graph theory perspective, motivated by applications in network design. Some of the applications of this project include efficient resource allocation, efficient controller, improving routing efficiency, and fault tolerance.

The algorithms we implement and use in our experiments include k-preserving subnet, recursive MA-ordering, MA-forests, and recursive MA-forests. We perform experiments on unweighted graphs by generating their minimum spanning tree variants and comparing the traversal rates, connectivities, message times, maximum load, and load for random messages. From our experiments, it is clear that the aforementioned applications can be satisfied without hampering the network performance significantly.

<p float="center" >
    <img src="images/original_graph.png" width="200"/>
    <img src="images/sparse_graph.png" width="200"/>
</p>

<p float="center" >
    <img src="images/message_time_plot.png" width="200"/>
    <img src="images/prob_wise_mincuts.png" width="200"/>
    <img src="images/congestion_plot.png" width="200"/>
</p>
