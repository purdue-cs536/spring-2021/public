<img src="../others/images/purdue-cs-logo.jpg" alt="drawing" width="450"/>

# Programming Assignments

Welcome to CS 536: Data Communication And Computer Networks! Through these programming assignments, you will gain hands-on experience with state-of-the-art real-world network programming. You will learn how to set up a virtual network of end-hosts and data-plane switches connected via custom topologies and controlled using production-ready control-plane operating systems. You will write a program that allows your computer to communicate with one another---be it across the room or the world. You will program a data-plane switch to collect and send network telemetry to the control plane, interpret it, and instruct the switch to drop or forward packets (like a Firewall or ACL).

The programming assignments are designed to be challenging but manageable in the time allotted. If you have questions, want to suggest clarifications, or struggle with any of the assignments this semester, please come to office hours and ask questions on [Campuswire](https://www.gradescope.com/courses/227978), or talk to an instructor before or after class.

> **Notes:** 
> - All assignments must be done individually, unless stated otherwise.
> - You are not allowed to copy or look at code from other students. However, you are welcome to discuss the assignments with other students without sharing code.


Let's get started!

#### [Assignment 0: Virtual Networks using Mininet and ONOS](assignment0)

#### [Assignment 1: File and Message Sharing using Sockets](assignment1)

#### [Assignment 2: DNS Reflection Mitigation using P4 and INT](assignment2)
