<img src="others/images/purdue-cs-logo.jpg" alt="drawing" width="450"/>

# CS 536: Data Communication And Computer Networks (Spring 2021)

[[_TOC_]]

## Logistics

- Instructor: [Muhammad Shahbaz](https://mshahbaz.gitlab.io/)
- Teaching assistants: [Tao Li](https://tao.li/)
- Lecture time: **MWF 9:30-10:30am**
- Location: Zoom (please find the Zoom link on [Brightspace](https://purdue.brightspace.com/d2l/home/209528) or [Campuswire](https://campuswire.com/c/G1FDC14E5))
- Credit Hours: 3.00
- Course discussion and announcements: [Campuswire](https://campuswire.com/c/G1FDC14E5) 
- Paper reviews: [Perusall](https://app.perusall.com/courses/purdue-cs536/_/dashboard)
- Project proposal, report, and link to a YouTube video (extra credit): [HotCRP](https://purdue-cs536-spr21.hotcrp.com/)
- Exam submission: [Gradescope](https://www.gradescope.com/courses/227978)
- Office hours
  - Friday 11:00am-12:00pm, Zoom, Muhammad Shahbaz
  - Monday 7:00-8:00pm, Zoom, Tao Li
- Practice study observation (PSO), Zoom, Tao Li
  - Monday 2:30-4:20pm
  - Wednesday 2:30-3:20pm
  - Friday 1:30-2:20pm

> **Note:** Visit [Brightspace](https://purdue.brightspace.com/d2l/home/209528) for instructions on joining [Campuswire](https://campuswire.com/c/G1FDC14E5), [Gradescope](https://www.gradescope.com/courses/227978), [Perusall](https://app.perusall.com/courses/purdue-cs536/_/dashboard), and [HotCRP](https://purdue-cs536-spr21.hotcrp.com/).

#### Suggesting edits to the course page and more ...

We strongly welcome any changes, updates, or corrections to the course page or assignments or else that you may have. Please submit them using the [GitLab merge request workflow](https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html).

## Course Description

CS536 is a graduate-level course in Computer Networks at Purdue University. In this course, we will explore the underlying principles and design decisions that have enabled the Internet to (inter)connect billions of people and trillions of things on and off this planet---especially under the current circumstances marred by COVID-19. We will study the pros and cons of the current Internet design, ranging from classical problems (e.g., packet switching, routing, naming, transport, and congestion control) to emerging and future trends, like data centers, software-defined networking (SDN), programmable data planes, and network function virtualization (NFV) to name a few.

The goals for this course are:

- To become familiar with the classical and emerging problems in networking and their solutions.
- To learn what's the state-of-the-art in networking research: network architecture, protocols, and systems.
To gain some practice in reading research papers and critically understanding others' research.
- To gain experience with network programming using state-of-the-art research platforms.

## Course Syllabus and Schedule

> **Notes:** 
> - This syllabus and schedule is preliminary and subject to change.
> - Everything is due at 11:59 PM (Eastern) on the given day.
> - Abbreviations refer to the following:
>   - KR: Kurose/Ross (6th edition)
>   - PD: Peterson/Davie (online version)
>   - GV: George Varghese (1st edition)


| Date    | Topics  | Notes | Readings |
| :------ | :------ | :------  | :------ |
| **Week 1** | **Course Overview** | | |
| Wed <br> Jan 20 | Introduction ([ppt](lectures/introduction.pptx), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=209528&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-2332383&srcou=209528)) | | [How to Read a Paper](readings/HowToRead2017.pdf) |
| Fri <br> Jan 22 | A Brief History of the Internet ([ppt](lectures/history.pptx), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=209528&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-2355919&srcou=209528)) | [Paper Review 1](readings/cerf74.pdf) `due Feb 03` <br> [Assignment 0](assignments) | [How to Review a Paper](https://greatresearch.org/2013/10/18/the-paper-reviewing-process/) <br> [Research Patterns](https://greatresearch.org/2013/09/20/research-patterns/) (Optional)</li></ul> |
| **Week 2** | **End Host and Home Networks I** | | |
| Mon <br> Jan 25 | Layering and Protocols ([ppt](lectures/layering.pptx), video) | | [End-to-End Arguments](readings/e2eArgument84.pdf) |
| Wed <br> Jan 27 | Sockets: The Network API ([ppt](lectures/sockets.pptx), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=209528&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-2388711&srcou=209528)) | | [Beej's Guide](http://beej.us/guide/bgnet/) |
| Fri <br> Jan 29 | Direct Links: The Wire Interface ([ppt](lectures/links.pptx), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=209528&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-2395383&srcou=209528)) | Team selections for the course project `due Feb 12` <br> [Assignment 1](assignments) `due Feb 12` | |
| **Week 3** | **End Host and Home Networks II** | | |
| Mon <br> Feb 01 | Indirect Links: Internetworking ([ppt](lectures/internetworking.pptx), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=209528&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-2415045&srcou=209528)) | | |
| Wed <br> Feb 03 | Transport: Process-to-Process Communication ([ppt](https://purdue.brightspace.com/d2l/le/content/209528/viewContent/5435149/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=209528&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-2427807&srcou=209528)) | | PD: [5.3 (RPC)](https://book.systemsapproach.org/e2e/rpc.html), [5.4 (RTP)](https://book.systemsapproach.org/e2e/rtp.html) |
| Fri <br> Feb 05 | Discuss Paper 1 ([ppt](lectures/paper1-discussion.pptx), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=209528&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-2437391&srcou=209528))  | [Paper Review 2](readings/BGPstability98.pdf) `due Feb 17` | |
| **Week 4** | **Wide Area Networks I** | | |
| Mon <br> Feb 08 | Direct Networks: Intradomain Routing ([ppt](https://purdue.brightspace.com/d2l/le/content/209528/viewContent/5473657/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=209528&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-2469092&srcou=209528)) | | PD: [3.4 (Routing)](https://book.systemsapproach.org/internetworking/routing.html) | | | |
| Wed <br> Feb 10 | Guest Lecture: The State-of-The-Art in End-Host Centric Networking by [Gianni Antichi](https://gianniantichi.github.io/) ([pdf](lectures/guests/gianni-antichi.pdf), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=209528&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-2469093&srcou=209528)) | | |
| Fri <br> Feb 12 | Indirect Networks: Interdomain Routing ([ppt](https://purdue.brightspace.com/d2l/le/content/209528/viewContent/5480259/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=209528&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-2484165&srcou=209528)) | Course Project Proposal `due Mar 08` <br> [Assignment 2](assignments) `due Mar 01` | PD: [4.1 (Global Internet)](https://book.systemsapproach.org/scaling/global.html) |
| **Week 5** | **Wide Area Networks II** | | |
| Mon <br> Feb 15 | Peering and IXPs ([ppt](https://purdue.brightspace.com/d2l/le/content/209528/viewContent/5490359/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=209528&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-2511401&srcou=209528)) | | [Where is Internet Congestion Occuring?](https://medium.com/network-insights/where-is-internet-congestion-occurring-e4333ed71168) |
| Wed <br> Feb 17 | *Reading Day:* No class | | |
| Fri <br> Feb 19 | Discuss Paper 2 ([ppt](lectures/paper2-discussion.pptx), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=209528&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-2531130&srcou=209528)) | [Paper Review 3](readings/Buffers04.pdf) `due Mar 03` | |
| **Week 6** | **Datacenter Networks** | | |
| Mon <br> Feb 22 | Origins and Architectures ([ppt](https://purdue.brightspace.com/d2l/le/content/209528/viewContent/5539809/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=209528&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-2566691&srcou=209528)) | | [MapReduce](https://static.googleusercontent.com/media/research.google.com/en//archive/mapreduce-osdi04.pdf) and an interview with Jeff Dean and Sanjay Ghemawat on why it came into being ([video](https://youtu.be/NXCIItzkn3E)) |
| Wed <br> Feb 24 | Application and Traffic Characteristics ([ppt](https://purdue.brightspace.com/d2l/le/content/209528/viewContent/5555716/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=209528&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-2566692&srcou=209528)) | | |
| Fri <br> Feb 26 | Inter-Datacenter Networking ([ppt](https://purdue.brightspace.com/d2l/le/content/209528/viewContent/5555721/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=209528&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-2566693&srcou=209528)) | | SD-WANs: [Google's B4](readings/b4-sigcomm13.pdf) and [Microsoft's SWAN](readings/swan-sigcomm13.pdf) |
| **Week 7** | **Resource Allocation I** | | |
| Mon <br> Mar 01 | *Assignment 2 Day*: No Class | | |
| Wed <br> Mar 03 | Transport: Congestion Control ([ppt](https://purdue.brightspace.com/d2l/le/content/209528/viewContent/5701805/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=209528&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-2589760&srcou=209528)) | | [BBR, the new kid on the TCP block](https://blog.apnic.net/2017/05/09/bbr-new-kid-tcp-block/) <br/> PD: [6.3 (TCP Congestion Control)](https://book.systemsapproach.org/congestion/tcpcc.html) |
| Fri <br> Mar 05 | Discuss Paper 3 ([ppt](lectures/paper3-discussion.pptx), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=209528&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-2607425&srcou=209528)) | [Paper Review 4](readings/queue14.pdf) `due Mar 17` <br> Course project report `due May 08` | |
| **Week 8** | **Resource Allocation II** | | |
| Mon <br> Mar 08 | Datacenter Networks: Load Balancing ([ppt](https://purdue.brightspace.com/d2l/le/content/209528/viewContent/5742365/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=209528&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-2630216&srcou=209528)) | | [CONGA: Distributed Congestion-Aware Load Balancing for Datacenters](https://people.csail.mit.edu/alizadeh/papers/conga-sigcomm14.pdf) |
| Wed <br> Mar 10 | Discuss Project Proposals ([ppt](https://purdue.brightspace.com/d2l/le/content/209528/viewContent/5742367/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=209528&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-2630219&srcou=209528)) | | |
| Fri <br> Mar 12 | Wide Area: Traffic Engineering ([ppt](https://purdue.brightspace.com/d2l/le/content/209528/viewContent/5745676/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=209528&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-2635157&srcou=209528)) | | [Famous operational issues (NANOG)](https://mailman.nanog.org/pipermail/nanog/2021-February/212082.html) |
| **Week 9** | **Software-Defined Networks I** | | |
| Mon <br> Mar 15 <br> 12:00pm - 12:50pm | Guest Lecture: Open Network Operating System (ONOS) by [Thomas Vachuska](https://opennetworking.org/?team=thomas-vachuska) ([ppt](https://purdue.brightspace.com/d2l/le/content/209528/viewContent/5762240/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=209528&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-2655426&srcou=209528)) | | |
| Wed <br> Mar 17 | Control-Plane Abstractions and OpenFlow ([ppt](https://purdue.brightspace.com/d2l/le/content/209528/viewContent/5762241/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=209528&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-2655431&srcou=209528)) | | How SDN will Shape Networking ([video](https://www.youtube.com/watch?v=c9-K5O_qYgA)) |
| Fri <br> Mar 19 | Discuss Paper 4 ([ppt](lectures/paper4-discussion.pptx), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=209528&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-2668923&srcou=209528)) | [Paper Review 5](readings/rmt-sigcomm13.pdf) `due Mar 31` | |
| **Week 10** | **Software-Defined Networks II** | | |
| Mon <br> Mar 22 | Network Virtualization ([ppt](https://purdue.brightspace.com/d2l/le/content/209528/viewContent/5799317/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=209528&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-2692728&srcou=209528)) | | [Network Virtualization in Multi-tenant Datacenters](https://www.usenix.org/system/files/conference/nsdi14/nsdi14-paper-koponen.pdf) |
| Wed <br> Mar 24 <br> 1:10pm - 2:00pm | Guest Lecture: Introduction to Virtual Networking by [Ben Pfaff](https://research.vmware.com/researchers/ben-pfaff) ([ppt](https://purdue.brightspace.com/d2l/le/content/209528/viewContent/5799327/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=209528&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-2692729&srcou=209528)) | | |
| Fri <br> Mar 26 | *Midterm Review* ([ppt](https://purdue.brightspace.com/d2l/le/content/209528/viewContent/5800664/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=209528&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-2694399&srcou=209528)) | | |
| **Week 11** |  **Programmable Networks I** | | |
| Mon <br> Mar 29 <br> 12:00pm - Tue <br> Mar 30 <br> 12:00pm | *Midterm Exam* | Exam Booklet: [ppt](https://purdue.brightspace.com/d2l/le/content/209528/viewContent/5807753/View) or [pdf](https://purdue.brightspace.com/d2l/le/content/209528/viewContent/5807759/View) | |
| Wed <br> Mar 31 <br> 2:30pm - 3:20pm | Guest Lecture: Mininet - Fun with Networking and (Lightweight) Virtualization by [Bob Lantz](http://www-cs-students.stanford.edu/~rlantz/) ([ppt](https://purdue.brightspace.com/d2l/le/content/209528/viewContent/5825260/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=209528&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-2721774&srcou=209528)) | | |
| Fri <br> Apr 02 | Discuss Paper 5 ([ppt](lectures/paper5-discussion.pptx), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=209528&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-2734126&srcou=209528)) | [Paper Review 6](readings/pepc-sigcomm2017.pdf) `due Apr 14` | |
| **Week 12** | **Programmable Networks II** | | |
| Mon <br> Apr 05 | Protocol-Independent Switching: Bottoms Up vs Top Down ([ppt](https://purdue.brightspace.com/d2l/le/content/209528/viewContent/5843376/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=209528&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-2744427&srcou=209528)) | | [PISCES: A Programmable, Protocol-Independent Software Switch](https://gitlab.com/mshahbaz/mshahbaz.gitlab.io/-/blob/master/publications/sigcomm16-pisces.pdf) |
| Wed <br> Apr 07 <br> 10:00am - 10:50am | Guest Lecture: Amazon AWS Networking - Advances & Challenges by [Ali Khayam](https://www.linkedin.com/in/alikhayam/) and [John Evans](https://www.linkedin.com/in/johnevansuk/) | | [Amazon AWS: SIGCOMM 2020 Lightning Talk](https://www.youtube.com/watch?v=ZFAzzwgUr6Y&list=PL9JNmYfQa0bgilL8p6xAnMFKbj8znSFx8&index=13) |
| Fri <br> Apr 09 | Router Design: Lookup and Scheduling ([ppt](https://purdue.brightspace.com/d2l/le/content/209528/viewContent/5866908/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=209528&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-2768849&srcou=209528)) | | GV: [10 (Exact-Match Lookups), 11 (Prefix-Match Lookups), 13 (Switching)](https://purdue-primo-prod.hosted.exlibrisgroup.com/primo-explore/fulldisplay?docid=TN_cdi_safari_books_9780120884773&context=PC&vid=PURDUE&lang=en_US&search_scope=everything&adaptor=primo_central_multiple_fe&tab=default_tab&query=any,contains,network%20algorithmics&offset=0) |
| **Week 13** | **Cellular Networks and 5G** | | |
| Mon <br> Apr 12 | Disaggregation at the Network Edge ([ppt](https://purdue.brightspace.com/d2l/le/content/209528/viewContent/6223421/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=209528&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-2809969&srcou=209528)) | | [5G Mobile Networks: A Systems Approach](https://5g.systemsapproach.org/) (Chapters: 1, 3, 6) |
| Wed <br> Apr 14 | Guest Lecture: A Low Latency and Consistent Cellular Control Plane by [Zafar Ayyub Qazi](http://web.lums.edu.pk/~zafar/) ([ppt](https://purdue.brightspace.com/d2l/le/content/209528/viewContent/6223423/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=209528&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-2809970&srcou=209528)) | | |
| Fri <br> Apr 16 | Discuss Paper 6 ([ppt](lectures/paper6-discussion.pptx), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=209528&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-2809971&srcou=209528)) | | |
| **Week 14** | **Cloud and Serverless Computing** | | |
| Mon <br> Apr 19 | Compute as a Utility ([ppt](https://purdue.brightspace.com/d2l/le/content/209528/viewContent/6223439/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=209528&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-2809992&srcou=209528)) | | [Above the Clouds: A Berkeley View of Cloud Computing](https://www2.eecs.berkeley.edu/Pubs/TechRpts/2009/EECS-2009-28.pdf) |
| Wed <br> Apr 21 | Function as a Service ([ppt](https://purdue.brightspace.com/d2l/le/content/209528/viewContent/6246808/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=209528&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-2832524&srcou=209528)) | | [Cloud Programming Simplified: A Berkeley View on Serverless Computing](https://www2.eecs.berkeley.edu/Pubs/TechRpts/2019/EECS-2019-3.pdf) |
| Fri <br> Apr 23 <br> 1:00pm - 1:50pm | Guest Lecture: P4 and Programmable Hardware by [Gordon Brebner](https://www.linkedin.com/in/gordonbrebner/) ([ppt](https://purdue.brightspace.com/d2l/le/content/209528/viewContent/6246809/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=209528&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-2832525&srcou=209528)) | | |
| **Week 15** | **ML for Networks** | | |
| Mon <br> Apr 26 <br> 10:30am - 11:20am | Guest Lecture: Do Switches Dream of Machine Learning by [Noa Zilberman](https://eng.ox.ac.uk/people/noa-zilberman/) ([ppt](https://purdue.brightspace.com/d2l/le/content/209528/viewContent/6262452/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=209528&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-2848858&srcou=209528)) | | |
| Wed <br> Apr 28 | Self-Driving Networks ([ppt](https://purdue.brightspace.com/d2l/le/content/209528/viewContent/6278894/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=209528&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-2864962&srcou=209528)) | | [Why (and How) Networks Should Run Themselves](https://arxiv.org/pdf/1710.11583.pdf) |
| Fri <br> Apr 30 | *Final Review* ([ppt](https://purdue.brightspace.com/d2l/le/content/209528/viewContent/6284666/View), [video](https://purdue.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=209528&type=lti&rcode=354644E0-4CD8-419D-A32F-4E78D8778E5C-2870334&srcou=209528))  | | |
| **Week 16** | **Exam Week**| | |
| Mon <br> May 03 <br> 12:00pm - Tue <br> May 04 <br> 12:00pm  | *Final Exam* | Exam Booklet: *emailed* | |
| Wed <br> May 05 | Project Presentations I | | |
| Fri <br> May 07 | Project Presentations II | | |

## Prerequisites

This course assumes that students have a basic understanding of data structures and algorithms and experience with programming languages like C/C++ and Python. Please see [CS 240](https://selfservice.mypurdue.purdue.edu/prod/bwckctlg.p_disp_course_detail?cat_term_in=202120&subj_code_in=CS&crse_numb_in=24000), [CS 380](https://selfservice.mypurdue.purdue.edu/prod/bwckctlg.p_disp_course_detail?cat_term_in=202120&subj_code_in=CS&crse_numb_in=38003), or similar courses at Purdue for reference.

## Recommended Textbooks
- Computer Networking: A Top-Down Approach by J. Kurose and K. Ross (6th Edition)
- Computer Networks: A Systems Approach by L. Peterson and B. Davie ([Online Version](https://book.systemsapproach.org/))
- Network Algorithmics: An Interdisciplinary Approach to Designing Fast Networked Devices (1st Edition), available online @ [Purdue Library](https://purdue-primo-prod.hosted.exlibrisgroup.com/primo-explore/fulldisplay?docid=TN_cdi_safari_books_9780120884773&context=PC&vid=PURDUE&lang=en_US&search_scope=everything&adaptor=primo_central_multiple_fe&tab=default_tab&query=any,contains,network%20algorithmics&offset=0)

> Other optional but interesting resources: [Software-Defined Networks: A Systems Approach - The New Network Stack](https://sdn.systemsapproach.org/) and [Sytems Approach - Blog](https://www.systemsapproach.org/blog).

## Programming Assignments

- [Assignment 0](assignments/assignment0): Virtual networks using Mininet and ONOS `not graded`
- [Assignment 1](assignments/assignment1): File and message sharing using Sockets `due Feb 12`
- [Assignment 2](assignments/assignment2): DNS Reflection Mitigation using P4 and INT `due Mar 01`

## Paper Reading and Discussion

Starting Week 3, we will be reading and discussing **six** research papers on topics ranging from network protocols, systems, and architectures. The class will take place on Friday, and we will be discussing one paper every other week -- so we can discuss them in-depth. You should closely read each paper and add comments and questions along with a 1-page summary of the paper on [Perusall](https://app.perusall.com/courses/purdue-cs536/_/dashboard) by the due date below. Please plan to provide at least five comments or questions for each paper on Perusall ahead of the associated class and follow the comments from other students and the course staff. Please come to class prepared with several points that will substantially contribute to the group discussion. 

> **Note:** General tips on reading papers are [here](readings/HowToRead05.pdf). 

Grades for your class participation and paper reviews will be determined based on attendance and, more importantly, substantial contributions to paper discussions both on Perusall and in class.

> **Note:** What we expect you to know and prepare before each discussion is [here](reading-papers.md).

### Reading list
- [Paper 1: A Protocol for Packet Network Intercommunication](readings/cerf74.pdf) `due Feb 03`
- [Paper 2: Internet Routing Instability](readings/BGPstability98.pdf) `due Feb 17`
- [Paper 3: Sizing Router Buffers](readings/Buffers04.pdf) `due Mar 03`
- [Paper 4: The Road to SDN: An Intellectual History of Programmable Networks](readings/queue14.pdf) `due Mar 17`
- [Paper 5: Forwarding Metamorphosis: Fast Programmable Match-Action Processing in Hardware for SDN](readings/rmt-sigcomm13.pdf) `due Mar 31`
- [Paper 6: A High Performance Packet Core for Next Generation Cellular Networks](readings/pepc-sigcomm2017.pdf) `due Apr 14`

## Midterm and Final Exams
There will be one midterm and a final exam based on course content (lectures, assignments, and paper readings).

- Midterm Exam `on Mar 29, 12:00pm EST` (take home, open book, 24-hours long)
- Final Exam `on May 03`

## Course Project

> **See the list of submitted projects [here](projects).**

The course includes a semester-long systems-building project that should be done in groups (either of size two or three, to be determined by the instructor after the course size is finalized) and must involve some programming. All group students are expected to share equally in the implementation. Students are expected to:

1. Reimplement or reproduce results from a paper we read during the semester or on a similar topic OR
2. Implement novel research projects, but such projects must be closely related to the material and topics covered in the course

> **Note:** If you are uncertain about the project's satisfiability, please contact the instructor as soon as possible.

### Project proposal
There are two types of projects in this class: reproducing others' research results and novel research. Proposals should be submitted via [HotCRP](https://purdue-cs536-spr21.hotcrp.com/).

#### a. Reproducing research
For reproducing research projects, students should include the following information in their proposal.

- **Background**
  - Name of the paper
  - A brief summary of the paper's problem domain/challenge, goals, and technical approach
  - Summary of paper's current implementation, evaluation strategy, and key results
- **Plan**
  - Proposed implementation (language, framework, etc.)
  - Evaluation strategy (testing platform/setup, simulated data/traces, etc.)
  - Key results trying to reproduce
  - Discussion of how you can compare your findings (quantitatively, qualitatively) with previously published results
  - New questions/settings trying to evaluate that are not addressed in the original paper
  - As the final plan mentions, it is important when reading a paper to ask what questions and/or settings are not included in an evaluation. For example, what happens if a workload shifts from being uniformly distributed to Zipfian? What if failures occur differently than evaluated? What if the data in a big data processing system has a different structure than evaluated (e.g., the "graph" that the data represents has a different edge distribution)? And so forth ...

#### b. Novel research
For novel research projects, students should include the following information.

- **Background**
  - What problem is research attempting to solve?
  - Why is the problem important?
  - How does this relate directly to topics or papers covered in class
- **Novelty**
  - What is the current state-of-the-art in related work, and why are they insufficient?
  - What is your (novel) technical insight/approach to solving this problem
- **Plan**
  - Proposed implementation (language, framework, etc.)
  - Key questions that evaluation will address
  - Evaluation strategy (testing platform/setup, simulated data/traces, etc.)
  - What does "success" look like? How do you quantify/compare results to alternative approaches / related work?

> **Note:** If students are concerned that their proposed project might not be sufficiently relevant to CS536 to satisfy the topical criteria, please contact instructors earlier than later. Proposals not closely related to the topical matter may be rejected outright as inappropriate.

### Project selection
The project proposal from above will be finalized. This may involve some back-and-forth between instructors and the group (likely via [Campuswire](https://campuswire.com/c/G1FDC14E5)).

### Final report
The final report will be in the form of a paper written in Latex and submitted as a PDF file via [HotCRP](https://purdue-cs536-spr21.hotcrp.com/).

> **Notes:** 
> - For reproduction projects, the report should have a particular focus and discussion on the evaluation (setup, comparative results, discussion of differences, and so forth).
> - The project writeup should be six pages of double-column, single-spaced, 10-point font (excluding references, which can go on extra pages), similar in spirit to a workshop paper. Papers must be typeset in LaTeX using the [ACM `sigconf` template](https://www.acm.org/binaries/content/assets/publications/consolidated-tex-template/acmart-master.zip).

### Deliverables

- Project proposal `due Mar 08`
- Project report `due May 08`
- Blogpost and/or YouTube video (extra credit) `due May 08`

## Grading

- Class participation: 3%
- Programming assignments: 10%
- Paper questions and summaries: 12%
- Midterm exam: 15%
- Final exam: 30%
- Course project: 30% (and 5% extra credit for video/demo posted on YouTube.)

## Policies

### Class participation

Lectures will be conducted via Zoom, with breakout rooms during paper discussions to enable a more lively discussion. We ask that students stay muted when not speaking. If you are comfortable having your video on and have sufficient bandwidth, that would be great to enable discussion. We also ask that you have your Zoom name correspond to the name you are using for the course so that the course staff can recognize each student.

### Late submission

- Grace period: 24 hours for the entire semester.
- After the grace period, 25% off for every 24 hours late, rounded up.

If you have extenuating circumstances that result in an assignment being late, please let us know about them as soon as possible.

### Academic integrity

We will default to Purdue's academic policies throughout this course unless stated otherwise. You are responsible for reading the pages linked below and will be held accountable for their contents.
- http://spaf.cerias.purdue.edu/integrity.html
- http://spaf.cerias.purdue.edu/cpolicy.html

### Honor code

By taking this course, you agree to take the [Purdue Honors Pledge](https://www.purdue.edu/odos/osrr/honor-pledge/about.html): "As a boilermaker pursuing academic excellence, I pledge to be honest and true in all that I do. Accountable together - we are Purdue."

### COVID-19 and quarantine + isolation!

Please visit [Protect Purdue Plan](https://protect.purdue.edu/plan/) or [Spring 2021 resources](https://www.purdue.edu/innovativelearning/teaching-remotely/resources.aspx) for most up-to-date guidelines and instructions.

## Acknowledgements

This class borrows inspirations from several incredible sources.

- The final project structure and accompanying instructions are inspired and adapted from my Ph.D. advisor, Jen Rexford's [COS 561](https://www.cs.princeton.edu/courses/archive/fall20/cos461/561.html) class of Fall 2020 at Princeton and Nick McKeown's [CS 244](http://web.stanford.edu/class/cs244/) class at Stanford.
- The course syllabus page format loosely follows Xin Jin's [EN.601.714](https://github.com/xinjin/course-adv-net) class at John Hopkins.
- The lecture slides' material is partially adapted from my Ph.D. advisors, Jen Rexford's [COS 561](https://www.cs.princeton.edu/courses/archive/fall20/cos461/561.html) class and Nick Feamster's [COS 461](https://www.cs.princeton.edu/courses/archive/spring19/cos461/) class at Princeton.
- [Programming assignment 1](assignments/assignment1) is based on a [similar assignment](https://github.com/PrincetonUniversity/COS461-Public/tree/master/assignments/assignment1) offered at Princeton by Nick Feamster.
- The course policies are partially adapted from Roopsha Samanta's [CS 560](https://www.cs.purdue.edu/homes/roopsha/cs560rap/s21/index.html) class at Purdue. 
